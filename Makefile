DUMMY: build
VERSION:=2.3

build:
	docker build -t tape-whale:$(VERSION) $(ARGS)
